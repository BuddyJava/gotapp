package com.timofeev.gotapp.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.redmadrobot.chronos.ChronosConnector;
import com.squareup.picasso.Picasso;
import com.timofeev.gotapp.R;
import com.timofeev.gotapp.data.database.ChronosFindOrInsertParent;
import com.timofeev.gotapp.data.database.ChronosLoadCharacterFromDb;
import com.timofeev.gotapp.data.managers.DataManager;
import com.timofeev.gotapp.data.storage.models.Aliase;
import com.timofeev.gotapp.data.storage.models.Character;
import com.timofeev.gotapp.data.storage.models.House;
import com.timofeev.gotapp.data.storage.models.Title;
import com.timofeev.gotapp.utils.AppConfig;
import com.timofeev.gotapp.utils.ChronosHelper;
import com.timofeev.gotapp.utils.ConstantManagers;

public class CharacterProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = ConstantManagers.TAG_PREFIX + " Profile";
    private static final String HOUSE_ID = "HOUSE_ID";
    private static final String CHARACTER_ID = "CHARACTER_ID";

    private CoordinatorLayout mCoordinatorLayout;
    private Toolbar mToolbar;
    private CollapsingToolbarLayout mToolbarLayout;
    private TextView mWords, mBorn, mTitles, mAliase;
    private Button mFather, mMother;
    private ImageView mImageView;
    private AppBarLayout mAppBarLayout;
    private LinearLayout mFatherLayout, mMotherLayout;
    private ChronosConnector mConnector;

    private Character mCharacter, mCharacterFather, mCharacterMother;
    private House mHouse;
    private DataManager mDataManager;
    private long mHouseId, mHouseIdFather = 0 , mHouseIdMother = 0;
    private long mCharacterId;
    private int mIcon;



    public static Intent newInstance(Context context,long houseId, long characterId){
        Log.d(TAG, "newInstance");
        Intent activityIntent = new Intent(context, CharacterProfileActivity.class);
        activityIntent.putExtra(HOUSE_ID, houseId);
        activityIntent.putExtra(CHARACTER_ID, characterId);
        return activityIntent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_charater);


        mHouseId = getIntent().getLongExtra(HOUSE_ID, AppConfig.STARK_HOUSE_ID);
        mCharacterId = getIntent().getLongExtra(CHARACTER_ID, 583);

        mDataManager = DataManager.getInstance();
        mConnector = new ChronosConnector();
        mConnector.onCreate(this, savedInstanceState);

        mCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.main_coordinator_container);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        mFatherLayout = (LinearLayout) findViewById(R.id.profile_father_ll);
        mMotherLayout = (LinearLayout) findViewById(R.id.profile_mother_ll);
        mWords = (TextView) findViewById(R.id.profile_words_tv);
        mBorn = (TextView) findViewById(R.id.profile_born_tv);
        mTitles = (TextView) findViewById(R.id.profile_titles_lv);
        mAliase = (TextView) findViewById(R.id.profile_aliase_tv);
        mImageView = (ImageView) findViewById(R.id.user_photo_img);
        mAppBarLayout = (AppBarLayout) findViewById(R.id.app_bar_layout);
        mAppBarLayout.setExpanded(true,true);

        mFather = (Button) findViewById(R.id.profile_father_btn);
        mMother = (Button) findViewById(R.id.profile_mother_btn);
        mFather.setOnClickListener(this);
        mMother.setOnClickListener(this);

        setDrawableInIcon(mHouseId);
        loadHouseFromDb();
        loadCharacterFromDb();
        setupToolbar();
        updateUi();
    }

    public void onOperationFinished(final ChronosFindOrInsertParent.Result result) {
        Log.d(TAG, "onOperationFinished");
        if (result.isSuccessful()) {
            if(result.getOutput().getCharacterId() == ChronosHelper.getIdFromUrl(mCharacter.getFather())){
                mCharacterFather = result.getOutput();
                mHouseIdFather = mCharacterFather.getHouseRelateId();
                mFather.setText(mCharacterFather.getName());
                motherChecked();
            }else{
                mCharacterMother = result.getOutput();
                mHouseIdMother = mCharacterMother.getHouseRelateId();
                mMother.setText(mCharacterMother.getName());
            }


        }
    }
    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.profile_father_btn){
            Intent fatherIntent;
            if(mHouseIdFather == 0){
                fatherIntent = CharacterProfileActivity.newInstance(this,mHouseId,
                        mCharacterFather.getCharacterId());
            }else{
                fatherIntent = CharacterProfileActivity.newInstance(this,mHouseIdFather,
                        mCharacterFather.getCharacterId());
            }
            startActivity(fatherIntent);
        }else {
            Intent motherIntent;
            if(mHouseIdMother == 0){
                motherIntent = CharacterProfileActivity.newInstance(this,mHouseId,
                        mCharacterMother.getCharacterId());
            }else {
                motherIntent = CharacterProfileActivity.newInstance(this,mHouseIdMother,
                        mCharacterMother.getCharacterId());
            }

            startActivity(motherIntent);

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mConnector.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mConnector.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mConnector.onSaveInstanceState(outState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            // TODO: 17.10.2016 доделать переход по стеку
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupToolbar() {
        Log.d(TAG, "setupToolbar");
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
            actionBar.setTitle(mCharacter.getName());
        }
    }

    private void showSnackbar(String message){
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    private void loadCharacterFromDb(){
        mCharacter = mDataManager.getCharacterFromDb(mCharacterId);

    }
    private void loadHouseFromDb() {
        mHouse = mDataManager.getHousesFromDb(mHouseId);

    }

    private void updateUi(){
//        Log.d(TAG, "updateUi");

        Picasso.with(this)
                .load(mIcon)
                .into(mImageView);

        mWords.setText(mHouse.getWords());
        mBorn.setText(mCharacter.getBorn());

        if(mCharacter.getTitles().size() != 0){
            String allTitles = "";
            for(Title title : mCharacter.getTitles()){
                allTitles += title.getTitle() + "\n";
            }
            mTitles.setText(allTitles);
        }
        else {
            mTitles.setText("");
        }

        if(mCharacter.getAliases().size() != 0){
            String allAlaies = "";
            for(Aliase aliase : mCharacter.getAliases()){
                allAlaies += aliase.getAliases() + "\n";
            }
            mAliase.setText(allAlaies);
        }
        else {
            mAliase.setText("");
        }
        mToolbarLayout.setTitle(mCharacter.getName());

        if(mCharacter.getFather().isEmpty()){
            mFatherLayout.setVisibility(View.GONE);
            motherChecked();
        }
        else{
            mConnector.runOperation(new ChronosFindOrInsertParent(ChronosHelper.
                    getIdFromUrl(mCharacter.getFather())), false);
        }

        if(!mCharacter.getDied().isEmpty()){
            showSnackbar("Died: " + mCharacter.getDied());
        }
    }

    private void setDrawableInIcon(long houseId){
        if(houseId == AppConfig.STARK_HOUSE_ID){
            mIcon = R.drawable.stark;
        }else if(houseId == AppConfig.LANNISTER_HOUSE_ID){
            mIcon = R.drawable.lannister;
        }else{
            mIcon = R.drawable.targarien;
        }
    }
    private void motherChecked(){
        if(mCharacter.getMother().isEmpty()){
            mMotherLayout.setVisibility(View.GONE);
        }else {
            mConnector.runOperation(new ChronosFindOrInsertParent(ChronosHelper.
                    getIdFromUrl(mCharacter.getMother())), false);
        }
    }
}
