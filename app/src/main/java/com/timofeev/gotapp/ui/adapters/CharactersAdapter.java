package com.timofeev.gotapp.ui.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.timofeev.gotapp.R;
import com.timofeev.gotapp.data.storage.models.Character;
import com.timofeev.gotapp.utils.AppConfig;

import java.util.List;


public class CharactersAdapter extends RecyclerView.Adapter<CharactersAdapter.CharactersViewHolder>{

    private List<Character> mCharacters ;
    private int icon = 0;
    private Context mContext;
    private CharactersViewHolder.CustomItemListener mItemListener;

    public CharactersAdapter(Context context, List<Character> characters, long houseId,
                             CharactersViewHolder.CustomItemListener customItemListener) {
        mCharacters = characters;
        mContext = context;
        mItemListener = customItemListener;
        if(houseId == Long.valueOf(AppConfig.STARK_HOUSE_ID)){
            this.icon = R.drawable.stark_icon;
        }else if(houseId == Long.valueOf(AppConfig.LANNISTER_HOUSE_ID)){
            this.icon = R.drawable.lanister_icon;
        }else{
            this.icon = R.drawable.targarien_icon;
        }
    }

    @Override
    public CharactersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_characters_list, parent, false);
        return new CharactersViewHolder(view, mItemListener);
    }

    @Override
    public void onBindViewHolder(CharactersViewHolder holder, int position) {
        Character character = mCharacters.get(position);

        holder.characterId = character.getCharacterId();
        Picasso.with(mContext)
                .load(icon)
                .resize(100,100)
                .placeholder(R.drawable.ic_brightness_1_black_24dp)
                .into(holder.mImageView);

        holder.mFullName.setText(character.getName());
        if(character.getTitles().size() != 0){
            String title = "";
            for(int i = 0; i < character.getTitles().size();i++){
                title += character.getTitles().get(i).getTitle() + ", ";
            }
            title = title.substring(0, title.length()-2);
            holder.mBio.setText(title);
            holder.mBio.setVisibility(View.VISIBLE);
        }else{
            if(character.getAliases().size() != 0){
                String aliase = "";
                for(int i = 0; i < character.getAliases().size();i++){
                    aliase += character.getAliases().get(i).getAliases() + ", ";
                }
                aliase = aliase.substring(0, aliase.length()-1);
                holder.mBio.setText(aliase);
                holder.mBio.setVisibility(View.VISIBLE);
            }else{
                holder.mBio.setVisibility(View.INVISIBLE);
            }
        }


    }

    @Override
    public int getItemCount() {
        return mCharacters.size();
    }



    public static class CharactersViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        protected ImageView mImageView;
        protected TextView mFullName, mBio;
        private CustomItemListener mListner ;
        private long characterId;
        public CharactersViewHolder(View itemView,CustomItemListener customItemListener) {
            super(itemView);
            mListner = customItemListener;
            itemView.setOnClickListener(this);
            mImageView = (ImageView) itemView.findViewById(R.id.character_image);
            mFullName = (TextView) itemView.findViewById(R.id.character_full_name_tv);
            mBio = (TextView) itemView.findViewById(R.id.character_bio_tv);

        }

        @Override
        public void onClick(View v) {

            if(mListner != null){
                mListner.OnClickHolder(getAdapterPosition(), characterId);
            }
        }

        public interface CustomItemListener{
            void OnClickHolder(int position, long charaterId);
        }
    }
}
