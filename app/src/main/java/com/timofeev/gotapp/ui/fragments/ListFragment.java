package com.timofeev.gotapp.ui.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.redmadrobot.chronos.ChronosConnector;
import com.timofeev.gotapp.R;
import com.timofeev.gotapp.data.database.ChronosLoadCharacterFromDb;
import com.timofeev.gotapp.data.database.ChronosLoadHouseFromDb;
import com.timofeev.gotapp.data.storage.models.Character;
import com.timofeev.gotapp.data.storage.models.House;
import com.timofeev.gotapp.ui.activities.CharacterListActivity;
import com.timofeev.gotapp.ui.activities.CharacterProfileActivity;
import com.timofeev.gotapp.ui.adapters.CharactersAdapter;
import com.timofeev.gotapp.utils.ConstantManagers;

import java.util.List;

public class ListFragment extends Fragment{


    private static final String TAG = ConstantManagers.TAG_PREFIX + " Fragment";
    private List<Character> mCharacters;
    private RecyclerView mRecyclerView;
    private static final String ARGS_ID_HOUSE = "houseId";
    private long houseId ;
    private ChronosConnector mConnector;

    public static ListFragment newInstance(long idHouse) {
        ListFragment fragment = new ListFragment();
        Bundle args = new Bundle();
        args.putLong(ARGS_ID_HOUSE, idHouse);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        mConnector = new ChronosConnector();
        mConnector.onCreate(this, savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");

        houseId = getArguments().getLong(ARGS_ID_HOUSE);
        View view = inflater.inflate(R.layout.fragment_characters_list, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.user_list_rv);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mConnector.runOperation(new ChronosLoadCharacterFromDb(houseId), false);
//        loadCharacterFromDb();

        return view;


    }

    public void onOperationFinished(final ChronosLoadCharacterFromDb.Result result) {
        Log.d(TAG, "onOperationFinished");
        if (result.isSuccessful()) {
            mCharacters = result.getOutput();
            loadCharacterFromDb();
        }
    }

    private void loadCharacterFromDb(){
        Log.d(TAG, "loadCharacterFromDb");

        if(mCharacters.size() == 0) {
            Log.d(TAG,"Size: " + mCharacters.size());
        }else{
            Log.d(TAG, "setAdapter");
            CharactersAdapter adapter = new CharactersAdapter(getActivity(), mCharacters, houseId,
                    new CharactersAdapter.CharactersViewHolder.CustomItemListener() {
                @Override
                public void OnClickHolder(int position, long characterId) {
                    Intent activityIntent = CharacterProfileActivity
                            .newInstance(getActivity(), houseId, characterId);
                    startActivity(activityIntent);
                }
            });
            mRecyclerView.setAdapter(adapter);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mConnector.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        mConnector.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mConnector.onSaveInstanceState(outState);
    }
}
