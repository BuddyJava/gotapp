package com.timofeev.gotapp.ui.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.redmadrobot.chronos.ChronosConnector;
import com.timofeev.gotapp.R;
import com.timofeev.gotapp.data.database.CronosSaveResponseToDb;
import com.timofeev.gotapp.data.managers.DataManager;
import com.timofeev.gotapp.data.networks.res.CharacterModelRes;
import com.timofeev.gotapp.data.networks.res.HouseModelRes;
import com.timofeev.gotapp.data.storage.models.Aliase;
import com.timofeev.gotapp.data.storage.models.AliaseDao;
import com.timofeev.gotapp.data.storage.models.Character;
import com.timofeev.gotapp.data.storage.models.CharacterDao;
import com.timofeev.gotapp.data.storage.models.House;
import com.timofeev.gotapp.data.storage.models.HouseDao;
import com.timofeev.gotapp.data.storage.models.Title;
import com.timofeev.gotapp.data.storage.models.TitleDao;
import com.timofeev.gotapp.utils.AppConfig;
import com.timofeev.gotapp.utils.ConstantManagers;
import com.timofeev.gotapp.utils.NetworkStatusCheker;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {

    private static final String TAG = ConstantManagers.TAG_PREFIX + " Splash";
    private DataManager mDataManager;

    private ChronosConnector mConnector;
    private ImageView imageView;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mDataManager = DataManager.getInstance();
        mConnector = new ChronosConnector();
        mConnector.onCreate(this, savedInstanceState);
        imageView = (ImageView) findViewById(R.id.imageview);
        mConnector.runOperation(new CronosSaveResponseToDb(), false);

    }

    public void onOperationFinished(final CronosSaveResponseToDb.Result result) {

        if (result.isSuccessful()) {
            //если все в порядке - переходим в главное активити
            if (result.getOutput() == ConstantManagers.DOWNLOAD_RESULT_OK){
                Intent intent = CharacterListActivity.newInstance(this);
                startActivity(intent);
            }
            if(result.getOutput() == ConstantManagers.DATABASE_RESULT_FULL){
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                    }
                }, ConstantManagers.DELAY_SIZE);
                nextActivity();
            }
            if (result.getOutput() == ConstantManagers.DISCONNECT_NETWORK){
                Toast.makeText(this,"Соединение с интернетом отсутсвует.",
                        Toast.LENGTH_LONG).show();
            }
        }
        else {
            Toast.makeText(this, "Error", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mConnector.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mConnector.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mConnector.onSaveInstanceState(outState);
    }

    private void nextActivity(){
        Intent intent = CharacterListActivity.newInstance(this);
        startActivity(intent);
    }
}
