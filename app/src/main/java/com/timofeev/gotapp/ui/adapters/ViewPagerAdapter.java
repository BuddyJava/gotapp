package com.timofeev.gotapp.ui.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.timofeev.gotapp.ui.fragments.ListFragment;
import com.timofeev.gotapp.utils.AppConfig;
import com.timofeev.gotapp.utils.ConstantManagers;


public class ViewPagerAdapter extends FragmentPagerAdapter{

    private final long[] HOUSE_ID = {AppConfig.STARK_HOUSE_ID,
            AppConfig.LANNISTER_HOUSE_ID,
            AppConfig.TARGARYEN_HOUSE_ID};

    private String[] pages = {ConstantManagers.HOUSE_STARKS,
            ConstantManagers.HOUSE_LANNISTERS,
            ConstantManagers.HOUSE_TARGARYANS};

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return pages[position];
    }

    @Override
    public Fragment getItem(int position) {
        return ListFragment.newInstance(HOUSE_ID[position]);
    }

    @Override
    public int getCount() {
        return HOUSE_ID.length;
    }
}
