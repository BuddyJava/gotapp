package com.timofeev.gotapp.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import com.timofeev.gotapp.R;
import com.timofeev.gotapp.data.managers.DataManager;
import com.timofeev.gotapp.data.storage.models.Character;
import com.timofeev.gotapp.data.storage.models.House;
import com.timofeev.gotapp.ui.adapters.CharactersAdapter;
import com.timofeev.gotapp.ui.adapters.ViewPagerAdapter;
import com.timofeev.gotapp.ui.fragments.ListFragment;
import com.timofeev.gotapp.utils.AppConfig;
import com.timofeev.gotapp.utils.ConstantManagers;

import java.util.List;

public class CharacterListActivity extends AppCompatActivity{

    private static final String TAG = ConstantManagers.TAG_PREFIX + " ListActivity";

    private DrawerLayout mDrawerLayout;
    private CoordinatorLayout mCoordinatorLayout;
    private Toolbar mToolbar;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private NavigationView mNavigationView;

    public static Intent newInstance(Context context){
        Intent activityIntent = new Intent(context, CharacterListActivity.class);
        return activityIntent;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character_list);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.main_coordinator_container);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        mTabLayout = (TabLayout) findViewById(R.id.tabs);
        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
                switch (tab.getPosition()){
                    case 0:
                        mNavigationView.getMenu().findItem(R.id.nav_starks).setChecked(true);
                        break;
                    case 1:
                        mNavigationView.getMenu().findItem(R.id.nav_lannisters).setChecked(true);
                        break;
                    case 2:
                        mNavigationView.getMenu().findItem(R.id.nav_targarians).setChecked(true);
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        setupToolbar();
        setupDrawer();
        setupViewPager();
        mTabLayout.setupWithViewPager(mViewPager);
//        loadHouseFromDb();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            mDrawerLayout.openDrawer(GravityCompat.START);
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupViewPager() {
        Log.d(TAG, "setupViewPager");
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void setupDrawer(){
        Log.d(TAG, "setupDrawer");
        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        if(mNavigationView != null){
            mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(MenuItem item) {
//                    Log.d(TAG, "onNavigationItemSelected");
                    switch (item.getItemId()){
                        case R.id.nav_starks:
                            mViewPager.setCurrentItem(0);
                            break;
                        case R.id.nav_lannisters:
                            mViewPager.setCurrentItem(1);
                            break;
                        case R.id.nav_targarians:
                            mViewPager.setCurrentItem(2);
                            break;
                    }
                    item.setChecked(true);
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                    return false;
                }
            });
        }
    }



    private void showSnackBar(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    private void setupToolbar() {
//        Log.d(TAG, "setupToolbar");
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
            actionBar.setTitle(getString(R.string.toolbar_title));
        }
    }


}

