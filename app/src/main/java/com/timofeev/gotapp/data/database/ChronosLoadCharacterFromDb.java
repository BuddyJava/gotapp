package com.timofeev.gotapp.data.database;


import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.redmadrobot.chronos.ChronosOperation;
import com.redmadrobot.chronos.ChronosOperationResult;
import com.timofeev.gotapp.data.managers.DataManager;
import com.timofeev.gotapp.data.storage.models.Character;
import com.timofeev.gotapp.data.storage.models.CharacterDao;
import com.timofeev.gotapp.data.storage.models.DaoSession;

import java.util.ArrayList;
import java.util.List;

public class ChronosLoadCharacterFromDb extends ChronosOperation<List<Character>> {
    private DaoSession mDaoSession;
    private long houseId;

    public ChronosLoadCharacterFromDb(long houseId) {
        this.houseId = houseId;
    }

    @Nullable
    @Override
    public List<Character> run() {

        mDaoSession = DataManager.getInstance().getDaoSession();

        List<Character> characters = new ArrayList<>();
        try {
            characters = mDaoSession.queryBuilder(Character.class)
                    .where(CharacterDao.Properties.HouseRelateId.eq(houseId))
                    .orderAsc(CharacterDao.Properties.Name)
                    .build().list();
        }catch (Exception e){
            e.getStackTrace();
        }
        return characters;
    }

    @NonNull
    @Override
    public Class<? extends ChronosOperationResult<List<Character>>> getResultClass() {
        return Result.class;
    }


    public final static class Result extends ChronosOperationResult<List<Character>> {
    }
}
