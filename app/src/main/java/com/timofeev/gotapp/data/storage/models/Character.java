package com.timofeev.gotapp.data.storage.models;

import com.timofeev.gotapp.data.networks.res.CharacterModelRes;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.JoinProperty;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.ToMany;
import org.greenrobot.greendao.annotation.Unique;

import java.util.List;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.DaoException;






@Entity (active = true, nameInDb = "CHARACTERS")
public class Character {

    @Id(autoincrement = true)
    private Long Id;

    @NotNull
    @Unique
    private Long characterId;

    @NotNull
    private Long houseRelateId;

    @NotNull
    @Unique
    private String name;

    private String gender;

    private String born;

    private String died;

    @ToMany(joinProperties = {
            @JoinProperty(name = "characterId", referencedName = "characterRelateId")
    })
    private List<Title> titles;

    @ToMany(joinProperties = {
            @JoinProperty(name = "characterId", referencedName = "characterRelateId")
    })
    private List<Aliase> aliases;

    private String father;

    private String mother;

    /** Used to resolve relations */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    @Generated(hash = 898307126)
    private transient CharacterDao myDao;

    public Character(CharacterModelRes modelRes, Long houseRelateId) {
        this.characterId = Long.valueOf(modelRes.getUrl().substring(modelRes.getUrl().lastIndexOf("/")+1,
                modelRes.getUrl().length()));
        this.houseRelateId = houseRelateId;
        this.name = modelRes.getName();
        this.gender = modelRes.getGender();
        this.born = modelRes.getBorn();
        this.died = modelRes.getDied();
        this.father = modelRes.getFather();
        this.mother = modelRes.getMother();
    }

    public Character(CharacterModelRes modelRes) {
        this.characterId = Long.valueOf(modelRes.getUrl().substring(modelRes.getUrl().lastIndexOf("/")+1,
                modelRes.getUrl().length()));
        this.houseRelateId =  0L;
        this.name = modelRes.getName();
        this.gender = modelRes.getGender();
        this.born = modelRes.getBorn();
        this.died = modelRes.getDied();
        this.father = modelRes.getFather();
        this.mother = modelRes.getMother();
    }

    @Generated(hash = 805942271)
    public Character(Long Id, @NotNull Long characterId, @NotNull Long houseRelateId,
            @NotNull String name, String gender, String born, String died, String father,
            String mother) {
        this.Id = Id;
        this.characterId = characterId;
        this.houseRelateId = houseRelateId;
        this.name = name;
        this.gender = gender;
        this.born = born;
        this.died = died;
        this.father = father;
        this.mother = mother;
    }

    @Generated(hash = 1853959157)
    public Character() {
    }

    public Long getId() {
        return this.Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public Long getCharacterId() {
        return this.characterId;
    }

    public void setCharacterId(Long characterId) {
        this.characterId = characterId;
    }

    public Long getHouseRelateId() {
        return this.houseRelateId;
    }

    public void setHouseRelateId(Long houseRelateId) {
        this.houseRelateId = houseRelateId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return this.gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBorn() {
        return this.born;
    }

    public void setBorn(String born) {
        this.born = born;
    }

    public String getDied() {
        return this.died;
    }

    public void setDied(String died) {
        this.died = died;
    }

    public String getFather() {
        return this.father;
    }

    public void setFather(String father) {
        this.father = father;
    }

    public String getMother() {
        return this.mother;
    }

    public void setMother(String mother) {
        this.mother = mother;
    }

    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 161493221)
    public List<Title> getTitles() {
        if (titles == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            TitleDao targetDao = daoSession.getTitleDao();
            List<Title> titlesNew = targetDao._queryCharacter_Titles(characterId);
            synchronized (this) {
                if (titles == null) {
                    titles = titlesNew;
                }
            }
        }
        return titles;
    }

    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    @Generated(hash = 1506933621)
    public synchronized void resetTitles() {
        titles = null;
    }

    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 1076928769)
    public List<Aliase> getAliases() {
        if (aliases == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            AliaseDao targetDao = daoSession.getAliaseDao();
            List<Aliase> aliasesNew = targetDao._queryCharacter_Aliases(characterId);
            synchronized (this) {
                if (aliases == null) {
                    aliases = aliasesNew;
                }
            }
        }
        return aliases;
    }

    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    @Generated(hash = 731614754)
    public synchronized void resetAliases() {
        aliases = null;
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 162219484)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getCharacterDao() : null;
    }

}
