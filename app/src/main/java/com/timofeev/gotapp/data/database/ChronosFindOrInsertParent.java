package com.timofeev.gotapp.data.database;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.redmadrobot.chronos.ChronosOperation;
import com.redmadrobot.chronos.ChronosOperationResult;
import com.timofeev.gotapp.data.managers.DataManager;
import com.timofeev.gotapp.data.networks.res.CharacterModelRes;
import com.timofeev.gotapp.data.storage.models.Aliase;
import com.timofeev.gotapp.data.storage.models.AliaseDao;
import com.timofeev.gotapp.data.storage.models.Character;
import com.timofeev.gotapp.data.storage.models.CharacterDao;
import com.timofeev.gotapp.data.storage.models.DaoSession;
import com.timofeev.gotapp.data.storage.models.Title;
import com.timofeev.gotapp.data.storage.models.TitleDao;
import com.timofeev.gotapp.utils.ChronosHelper;
import com.timofeev.gotapp.utils.GoTApplication;
import com.timofeev.gotapp.utils.NetworkStatusCheker;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;


public class ChronosFindOrInsertParent extends ChronosOperation<Character>{

    private DaoSession mDaoSession;
    private long characterId;
    private DataManager mDataManager;
    private CharacterDao mCharacterDao;
    private TitleDao mTitleDao;
    private AliaseDao mAliaseDao;

    public ChronosFindOrInsertParent(long characterId) {
        this.characterId = characterId;

    }

    @Nullable
    @Override
    public Character run() {
        mDataManager = DataManager.getInstance();
        mDaoSession = mDataManager.getDaoSession();

        mCharacterDao = mDaoSession.getCharacterDao();
        mTitleDao = mDaoSession.getTitleDao();
        mAliaseDao = mDaoSession.getAliaseDao();


        Character character = new Character();
        try{
            character = mDaoSession.queryBuilder(Character.class)
                    .where(CharacterDao.Properties.CharacterId.eq(characterId))
                    .build().unique();
        }catch (Exception e){
            e.printStackTrace();
        }
        if(character == null){
            if(NetworkStatusCheker.isNetworkAvailable(GoTApplication.getContext())){
                try{
                    Call<CharacterModelRes> call = mDataManager.getCharacter(characterId);
                    Response<CharacterModelRes> response = call.execute();
                    if(response.code() == 200){
                        character = new Character(response.body());
                        if(!character.getTitles().isEmpty()){
                            mTitleDao.insertOrReplaceInTx(ChronosHelper.
                                    getTitleListFromCharacterRes(response.body()));
                        }
                        if(!character.getAliases().isEmpty()){
                            mAliaseDao.insertOrReplaceInTx(ChronosHelper.
                                    getAliaseListFromCharacterRes(response.body()));
                        }
                        mCharacterDao.insertOrReplace(character);
                    }
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
            else{
                return null;
            }
        }
        return character;
    }

    @NonNull
    @Override
    public Class<? extends ChronosOperationResult<Character>> getResultClass() {
        return Result.class;
    }

    public final static class Result extends ChronosOperationResult<Character> {
    }


}
