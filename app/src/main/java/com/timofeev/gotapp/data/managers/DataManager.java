package com.timofeev.gotapp.data.managers;

import android.content.Context;

import com.timofeev.gotapp.data.networks.RestService;
import com.timofeev.gotapp.data.networks.ServiceGenerator;
import com.timofeev.gotapp.data.networks.res.CharacterModelRes;
import com.timofeev.gotapp.data.networks.res.HouseModelRes;
import com.timofeev.gotapp.data.storage.models.Character;
import com.timofeev.gotapp.data.storage.models.CharacterDao;
import com.timofeev.gotapp.data.storage.models.DaoSession;
import com.timofeev.gotapp.data.storage.models.House;
import com.timofeev.gotapp.data.storage.models.HouseDao;
import com.timofeev.gotapp.utils.ConstantManagers;
import com.timofeev.gotapp.utils.GoTApplication;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

public class DataManager {

    private static final String TAG = ConstantManagers.TAG_PREFIX + " DataManager";

    private static volatile DataManager INSTANCE = null;
    private RestService mRestService;
    private DaoSession mDaoSession;


    private DataManager() {
        mRestService = ServiceGenerator.createService(RestService.class);
        mDaoSession = GoTApplication.getDaoSession();
    }

    public static DataManager getInstance(){
        DataManager localInstance = INSTANCE;
        if (localInstance == null) {
            synchronized (DataManager.class) {
                localInstance = INSTANCE;
                if (localInstance == null) {
                    INSTANCE = localInstance = new DataManager();
                }
            }
        }
        return localInstance;
    }

    public DaoSession getDaoSession() {
        return mDaoSession;
    }

    /* region ======= Network ========*/
    public Call<HouseModelRes> getHouse(long houseId){
        return mRestService.getHouse(houseId);
    }

    public Call<CharacterModelRes> getCharacter(long characterId){
        return mRestService.getCharacter(characterId);
    }
    /* endregion ======= Network ========*/

    /* region ======= Database ========*/
        public Character getCharacterFromDb(long characterId){
            Character character = new Character();
            try{
                character = mDaoSession.queryBuilder(Character.class)
                        .where(CharacterDao.Properties.CharacterId.eq(characterId))
                        .build().unique();
            }
            catch (Exception e){
                e.getStackTrace();
            }
            return character;
        }
    public House getHousesFromDb(long houseId){
            House house = new House();
            try{
                house = mDaoSession.queryBuilder(House.class)
                        .where(HouseDao.Properties.HouseId.eq(houseId))
                        .build().unique();
            }
            catch (Exception e){
                e.getStackTrace();
            }
            return house;
        }
    public List<House> getHousesListFromDb(){
            List<House> houses = new ArrayList<>();
            try{
                houses = mDaoSession.queryBuilder(House.class)
                        .build().list();
            }
            catch (Exception e){
                e.getStackTrace();
            }
            return houses;
        }

    /* endregion ======= Database ========*/
}
