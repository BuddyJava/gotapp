package com.timofeev.gotapp.data.storage.models;


import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.DaoException;



@Entity(active = true, nameInDb = "ALIASES")
public class Aliase {

    @Id(autoincrement = true)
    private Long AliaseId;

    @NotNull
    private Long characterRelateId;

    private String aliases;

    /** Used to resolve relations */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    @Generated(hash = 947773599)
    private transient AliaseDao myDao;


    public Aliase(String aliases, Long characterRelateId) {
        this.aliases = aliases;
        this.characterRelateId = characterRelateId;
    }


    @Generated(hash = 2146941642)
    public Aliase(Long AliaseId, @NotNull Long characterRelateId, String aliases) {
        this.AliaseId = AliaseId;
        this.characterRelateId = characterRelateId;
        this.aliases = aliases;
    }


    @Generated(hash = 1566368553)
    public Aliase() {
    }


    public Long getAliaseId() {
        return this.AliaseId;
    }


    public void setAliaseId(Long AliaseId) {
        this.AliaseId = AliaseId;
    }


    public Long getCharacterRelateId() {
        return this.characterRelateId;
    }


    public void setCharacterRelateId(Long characterRelateId) {
        this.characterRelateId = characterRelateId;
    }


    public String getAliases() {
        return this.aliases;
    }


    public void setAliases(String aliases) {
        this.aliases = aliases;
    }


    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }


    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }


    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }


    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 1907117852)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getAliaseDao() : null;
    }

}
