package com.timofeev.gotapp.data.database;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.redmadrobot.chronos.ChronosOperation;
import com.redmadrobot.chronos.ChronosOperationResult;
import com.timofeev.gotapp.data.managers.DataManager;
import com.timofeev.gotapp.data.networks.res.CharacterModelRes;
import com.timofeev.gotapp.data.networks.res.HouseModelRes;
import com.timofeev.gotapp.data.storage.models.Aliase;
import com.timofeev.gotapp.data.storage.models.AliaseDao;
import com.timofeev.gotapp.data.storage.models.Character;
import com.timofeev.gotapp.data.storage.models.CharacterDao;
import com.timofeev.gotapp.data.storage.models.House;
import com.timofeev.gotapp.data.storage.models.HouseDao;
import com.timofeev.gotapp.data.storage.models.Title;
import com.timofeev.gotapp.data.storage.models.TitleDao;
import com.timofeev.gotapp.utils.AppConfig;
import com.timofeev.gotapp.utils.ChronosHelper;
import com.timofeev.gotapp.utils.ConstantManagers;
import com.timofeev.gotapp.utils.GoTApplication;
import com.timofeev.gotapp.utils.NetworkStatusCheker;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Response;

public class CronosSaveResponseToDb extends ChronosOperation<Integer> {

    private static final String TAG = ConstantManagers.TAG_PREFIX + "cronos";
    private DataManager mDataManager;
    private Context mContext;

    private HouseDao mHouseDao;
    private CharacterDao mCharacterDao;
    private TitleDao mTitleDao;
    private AliaseDao mAliaseDao;

    private static final long[] HOUSE_ID ={AppConfig.STARK_HOUSE_ID,
            AppConfig.LANNISTER_HOUSE_ID,AppConfig.TARGARYEN_HOUSE_ID};

    @Nullable
    @Override
    public Integer run() {
        mContext = GoTApplication.getContext();
        mDataManager = DataManager.getInstance();


        if(mDataManager.getHousesListFromDb().size() == 0){

            mHouseDao = mDataManager.getDaoSession().getHouseDao();
            mCharacterDao = mDataManager.getDaoSession().getCharacterDao();
            mTitleDao = mDataManager.getDaoSession().getTitleDao();
            mAliaseDao = mDataManager.getDaoSession().getAliaseDao();

            if(NetworkStatusCheker.isNetworkAvailable(mContext)){
                List<House> houseList = new ArrayList<>();
                List<Character> characterList = new ArrayList<>();
                List<Title> titleList = new ArrayList<>();
                List<Aliase> aliaseList = new ArrayList<>();

                for(long id : HOUSE_ID){
                    try{
                        Call<HouseModelRes> call = mDataManager.getHouse(id);
                        Response<HouseModelRes> res = call.execute();
                        if (res.code() == 200){
                            houseList.add(new House(res.body()));
                            for(String swornMember : res.body().getSwornMembers()) {
                                Call<CharacterModelRes> characterModelResCall =
                                        mDataManager.getCharacter(ChronosHelper
                                                .getIdFromUrl(swornMember));
                                Response<CharacterModelRes> response = characterModelResCall.execute();
                                if(response.code() == 200){
                                    CharacterModelRes model = response.body();
                                    if(model.getTitles().size() != 0){
                                        titleList.addAll(ChronosHelper
                                                .getTitleListFromCharacterRes(model));
                                    }
                                    if(response.body().getAliases().size() != 0){
                                        aliaseList.addAll(ChronosHelper
                                                .getAliaseListFromCharacterRes(model));
                                    }
                                    Log.d(TAG, "addCharactList");
                                    characterList.add(new Character(model, id));
                                    Log.d(TAG, "addCharactList:end");
                                }
                            }
                        }else {
                            Log.d(TAG, "Error house load");
                        }
                    }catch (IOException e){
                        e.getStackTrace();
                    }
                }
                Log.d(TAG, "saveInDb");
                mHouseDao.insertOrReplaceInTx(houseList);
                mCharacterDao.insertOrReplaceInTx(characterList);
                mAliaseDao.insertOrReplaceInTx(aliaseList);
                mTitleDao.insertOrReplaceInTx(titleList);
            }
            else {
                return ConstantManagers.DISCONNECT_NETWORK;
            }
            return ConstantManagers.DOWNLOAD_RESULT_OK;
        }
        else {
            return ConstantManagers.DATABASE_RESULT_FULL;
        }
    }

    @NonNull
    @Override
    public Class<? extends ChronosOperationResult<Integer>> getResultClass() {
        return Result.class;
    }

    public final static class Result extends ChronosOperationResult<Integer> {

    }
}
