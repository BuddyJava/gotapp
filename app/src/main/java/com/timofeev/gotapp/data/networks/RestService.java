package com.timofeev.gotapp.data.networks;

import com.timofeev.gotapp.data.networks.res.CharacterModelRes;
import com.timofeev.gotapp.data.networks.res.HouseModelRes;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface RestService {

    @GET("houses/{id}")
    Call<HouseModelRes> getHouse(@Path("id") long houseId);

    @GET("characters/{id}")
    Call<CharacterModelRes> getCharacter(@Path("id") long characterId);
}
