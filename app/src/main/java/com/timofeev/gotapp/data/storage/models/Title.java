package com.timofeev.gotapp.data.storage.models;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;

import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.DaoException;


@Entity(active = true, nameInDb = "TITLES")
public class Title {

    @Id(autoincrement = true)
    private Long titleId;

    @NotNull
    private Long characterRelateId;

    private String title;

    /** Used to resolve relations */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    @Generated(hash = 1794908068)
    private transient TitleDao myDao;


    public Title(String title, Long characterRelateId) {
        this.title = title;
        this.characterRelateId = characterRelateId;
    }


    @Generated(hash = 817381889)
    public Title(Long titleId, @NotNull Long characterRelateId, String title) {
        this.titleId = titleId;
        this.characterRelateId = characterRelateId;
        this.title = title;
    }


    @Generated(hash = 177602963)
    public Title() {
    }


    public Long getTitleId() {
        return this.titleId;
    }


    public void setTitleId(Long titleId) {
        this.titleId = titleId;
    }


    public Long getCharacterRelateId() {
        return this.characterRelateId;
    }


    public void setCharacterRelateId(Long characterRelateId) {
        this.characterRelateId = characterRelateId;
    }


    public String getTitle() {
        return this.title;
    }


    public void setTitle(String title) {
        this.title = title;
    }


    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }


    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }


    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }


    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 1326705651)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getTitleDao() : null;
    }

}
