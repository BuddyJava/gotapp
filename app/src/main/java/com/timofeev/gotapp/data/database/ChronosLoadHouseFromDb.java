package com.timofeev.gotapp.data.database;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.redmadrobot.chronos.ChronosOperation;
import com.redmadrobot.chronos.ChronosOperationResult;
import com.timofeev.gotapp.data.managers.DataManager;
import com.timofeev.gotapp.data.storage.models.CharacterDao;
import com.timofeev.gotapp.data.storage.models.DaoSession;
import com.timofeev.gotapp.data.storage.models.House;

public class ChronosLoadHouseFromDb extends ChronosOperation<House>{

        private DaoSession mDaoSession;
        private long houseId;

        public ChronosLoadHouseFromDb(long houseId) {
            this.houseId = houseId;
        }

        @Nullable
        @Override
        public House run() {

            mDaoSession = DataManager.getInstance().getDaoSession();

            House house = new House();
            try {
                house = mDaoSession.queryBuilder(House.class)
                        .where(CharacterDao.Properties.HouseRelateId.eq(houseId))
                        .build().unique();
            }catch (Exception e){
                e.getStackTrace();
            }

            return house;
        }

        @NonNull
        @Override
        public Class<? extends ChronosOperationResult<House>> getResultClass() {
            return Result.class;
        }


        public final static class Result extends ChronosOperationResult<House> {
        }
}
