package com.timofeev.gotapp.data.storage.models;


import com.timofeev.gotapp.data.networks.res.HouseModelRes;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.JoinProperty;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.ToMany;
import org.greenrobot.greendao.annotation.Unique;

import java.util.List;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.DaoException;




@Entity(active = true, nameInDb = "HOUSES")
public class House {

    @Id(autoincrement = true)
    private Long id;

    @NotNull
    @Unique
    private Long houseId;

    @NotNull
    @Unique
    private String houseName;

    @NotNull
    @Unique
    private String searchName;

    private String words;

    @ToMany(joinProperties = {
            @JoinProperty(name="houseId", referencedName = "houseRelateId")
    })
    private List<Character> characters;


    public House(HouseModelRes houseModelRes) {
        this.houseId = Long.valueOf(houseModelRes.getUrl().substring(houseModelRes.getUrl().lastIndexOf("/")+1,
                houseModelRes.getUrl().length()));
        this.houseName = houseModelRes.getName();
        this.searchName = houseModelRes.getName().toUpperCase();
        this.words = houseModelRes.getWords();
    }
    /** Used to resolve relations */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    @Generated(hash = 1167916919)
    private transient HouseDao myDao;



    @Generated(hash = 1528764853)
    public House(Long id, @NotNull Long houseId, @NotNull String houseName,
            @NotNull String searchName, String words) {
        this.id = id;
        this.houseId = houseId;
        this.houseName = houseName;
        this.searchName = searchName;
        this.words = words;
    }

    @Generated(hash = 389023854)
    public House() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getHouseId() {
        return this.houseId;
    }

    public void setHouseId(Long houseId) {
        this.houseId = houseId;
    }

    public String getHouseName() {
        return this.houseName;
    }

    public void setHouseName(String houseName) {
        this.houseName = houseName;
    }

    public String getSearchName() {
        return this.searchName;
    }

    public void setSearchName(String searchName) {
        this.searchName = searchName;
    }

    public String getWords() {
        return this.words;
    }

    public void setWords(String words) {
        this.words = words;
    }

    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 1572715877)
    public List<Character> getCharacters() {
        if (characters == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            CharacterDao targetDao = daoSession.getCharacterDao();
            List<Character> charactersNew = targetDao._queryHouse_Characters(houseId);
            synchronized (this) {
                if (characters == null) {
                    characters = charactersNew;
                }
            }
        }
        return characters;
    }

    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    @Generated(hash = 1728304228)
    public synchronized void resetCharacters() {
        characters = null;
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 451323429)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getHouseDao() : null;
    }


}
