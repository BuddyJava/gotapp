package com.timofeev.gotapp.utils;


import android.support.annotation.NonNull;

import com.timofeev.gotapp.data.networks.res.CharacterModelRes;
import com.timofeev.gotapp.data.storage.models.Aliase;
import com.timofeev.gotapp.data.storage.models.Title;

import java.util.ArrayList;
import java.util.List;

public class ChronosHelper {
    public static List<Title> getTitleListFromCharacterRes(CharacterModelRes characterModelRes){
        List<Title> titles = new ArrayList<>();
        for(String title : characterModelRes.getTitles()){
            titles.add(new Title(title,
                    getIdFromUrl(characterModelRes.getUrl())));
        }
        return titles;
    }
    public   static  List<Aliase> getAliaseListFromCharacterRes(CharacterModelRes characterModelRes) {
        List<Aliase> aliases = new ArrayList<>();
        for (String aliase : characterModelRes.getAliases()) {
            aliases.add(new Aliase(aliase, getIdFromUrl(characterModelRes.getUrl())));
        }
        return aliases;
    }
    @NonNull
    public static Long getIdFromUrl(String url){
        return Long.valueOf(url.substring(url.lastIndexOf("/")+1,url.length()));

    }
}
