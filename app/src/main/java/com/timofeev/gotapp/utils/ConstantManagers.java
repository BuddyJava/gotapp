package com.timofeev.gotapp.utils;

public interface ConstantManagers {


    String TAG_PREFIX = "GOT";
    String HOUSE_STARKS = "STARKS";
    String HOUSE_TARGARYANS = "TARGARYANS";
    String HOUSE_LANNISTERS = "LANNISTERS";

    int DOWNLOAD_RESULT_OK = 0;
    int DATABASE_RESULT_FULL = 1;
    int DISCONNECT_NETWORK = 2;

    int DELAY_SIZE = 3000;
}
