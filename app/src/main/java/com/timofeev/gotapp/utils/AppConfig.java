package com.timofeev.gotapp.utils;



public interface AppConfig {

    long  LANNISTER_HOUSE_ID = 229;
    long STARK_HOUSE_ID = 362;
    long TARGARYEN_HOUSE_ID = 378;

    String BASE_URL = "http://anapioficeandfire.com/api/";

    int MAX_READ_TIMEOUT = 5000;
    int MAX_CONNECT_TIMEOUT = 5000;
}
