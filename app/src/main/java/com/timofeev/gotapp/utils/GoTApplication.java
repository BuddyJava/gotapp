package com.timofeev.gotapp.utils;

import android.app.Application;
import android.content.Context;

import com.facebook.stetho.Stetho;
import com.timofeev.gotapp.data.storage.models.DaoMaster;
import com.timofeev.gotapp.data.storage.models.DaoSession;

import org.greenrobot.greendao.database.Database;


public class GoTApplication extends Application{

    private static Context mContext;
    private static DaoSession sDaoSession;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();

        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, Schema.NAME_DB);
        Database db = helper.getWritableDb();
        sDaoSession = new DaoMaster(db).newSession();

        Stetho.initializeWithDefaults(this);
    }

    public static Context getContext(){
        return mContext;
    }

    public static DaoSession getDaoSession() {
        return sDaoSession;
    }
}
